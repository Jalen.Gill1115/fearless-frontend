import React from 'react';

class PresentationForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            companyName: '',
            title: '',
            synopsis: '',
            conferences: []
        }
        this.HandleNameChange = this.HandleNameChange.bind(this)
        this.HandleEmailChange = this.HandleEmailChange.bind(this)
        this.HandleCompanyNameChange = this.HandleCompanyNameChange.bind(this)
        this.HandleTitleChange = this.HandleTitleChange.bind(this)
        this.HandleSynopsisChange = this.HandleSynopsisChange.bind(this)
        this.HandleConferencesChange = this.HandleConferencesChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    HandleNameChange(event) {
        const value = event.target.value
        this.setState({name: value})
    }

    HandleEmailChange(event) {
        const value = event.target.value
        this.setState({email: value})
    }

    HandleCompanyNameChange(event) {
        const value = event.target.value
        this.setState({companyName: value})
    }

    HandleTitleChange(event) {
        const value = event.target.value
        this.setState({title: value})
    }

    HandleSynopsisChange(event) {
        const value = event.target.value
        this.setState({synopsis: value})
    }

    HandleConferencesChange(event) {
        const value = event.target.value
        this.setState({conference: value})
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.company_name = data.companyName;
        delete data.companyName;
        delete data.conferences;
        console.log(data);

        const presentationUrl = 'http://localhost:8000/api/presentations/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json();
            console.log(newPresentation);

            const cleared = {
            name: '',
            email: '',
            companyName: '',
            title: '',
            synopsis: '',
            conference: ''
            };
            this.setState(cleared);
        }
    }
    async componentDidMount() {
        const url = 'http://localhost:8000/api/conferences/';

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({
                conferences: data.conferences
            });
        }
    }



    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1>Create a new presentation</h1>
                            <form onSubmit={this.handleSubmit} id="create-presentation-form">
                            <div className="form-floating mb-3">
                                <input value={this.state.name} onChange={this.HandleNameChange} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                                <label htmlFor="presenter_name">Presenter name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.email} onChange={this.HandleEmailChange} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                                <label htmlFor="presenter_email">Presenter email</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.companyName} onChange={this.HandleCompanyNameChange} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"/>
                                <label htmlFor="company_name">Company name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.title} onChange={this.HandleTitleChange} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
                                <label htmlFor="title">Title</label>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="synopsis">Synopsis</label>
                                <textarea value={this.state.synopsis} onChange={this.HandleSynopsisChange} className="form-control" id="synopsis" rows="3" name="synopsis" ></textarea>
                            </div>
                            <div className="mb-3">
                                <select value={this.state.conference} onChange={this.HandleConferencesChange} required name="conference" id="conference" className="form-select">
                                <option value="">Choose a conference</option>
                                {this.state.conferences.map(conference => {
                                            return (
                                            <option key={conference.id} value={conference.id}>
                                                {conference.name}
                                            </option>
                                            )
                                            })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )

    }
}


export default PresentationForm;
